// Types

/**
 * @typedef {Object} WebNavigationEventChild
 * @property {Object} detail
 * @property {Object} detail.state
 * @property {string} detail.url
 *
 * @typedef {Event & WebNavigationEventChild} WebNavigationEvent
 */

/**
 * @typedef {Object} FoundParam
 * @property {string} key
 * @property {string} value
 */

// Utils Functions

/**
 * Pushes URL to a new location without reloading page
 * @param {string} url URL you want to navigate to
 * @param {*} state Any aditional data you may want to obtain back after the route has changed
 * @returns {void}
 */
function navigate(url, state = {}) {
  const pushChangeEvent = new CustomEvent("webnavigation", {
    detail: {
      state,
      url,
    },
  });
  document.dispatchEvent(pushChangeEvent);
  history.pushState({}, "", url);
}

/**
 * Listens for the change in URL from the client-side navigation
 * @param {(e: WebNavigationEvent) => void} callback
 * @returns {void}
 */
function listenNavigation(callback) {
  document.addEventListener("webnavigation", callback, false);
}

/**
 * Get the value from URL parameters
 * @param {string} key
 * @returns {string} value associated to the key
 */
function getParam(key) {
  /** @type {WebRouter} */
  const router = document.querySelector("web-router");
  return router?.foundParams?.find?.((param) => param.key === key)?.value || null;
}

// Components

class WebRouter extends HTMLElement {
  /** @type {FoundParam[]} */
  foundParams;

  constructor() {
    super();
  }

  connectedCallback() {
    this.updateActiveRoute(window.location.pathname);
    listenNavigation((e) => {
      this.updateActiveRoute(e?.detail?.url);
    });
  }

  /**
   * Updates which route is active or not
   * @param {string} newUrl
   */
  updateActiveRoute(newUrl) {
    let found = false;
    const routes = this.querySelectorAll("web-route");
    routes.forEach((thisRoute) => {
      // Reset all routes at first
      thisRoute.removeAttribute("active");

      // No need to search if routes has already been found
      if (found) return;
      const path = thisRoute?.getAttribute("path");

      // Exact match can return immediately
      if (newUrl === path) {
        thisRoute.setAttribute("active", "true");
        found = true;
        return;
      }

      // If no params to analyze, no need to proceed
      const params = path?.match(/:[A-Za-z]+/g);
      if (!params?.length) return;

      const sectionsUrl = newUrl?.split("/");
      sectionsUrl?.shift();
      const sectionsPath = path?.split("/");
      sectionsPath?.shift();

      // Number of sections between "/" msut be the same as parametes to match
      if (sectionsUrl.length !== sectionsPath.length) return;

      // Adding the parameters and their values to be saved globally to the router
      this.foundParams = params.map((param) => {
        const index = sectionsPath.indexOf(param);
        const value = sectionsUrl[index];
        return {
          key: param?.replace(":", ""),
          value: value,
        };
      });

      thisRoute.setAttribute("active", "true");
      found = true;
      return;
    });
  }
}

class WebRoute extends HTMLElement {
  /** @type {boolean} */
  active;

  constructor() {
    super();
    const active = this.getAttribute("active");
    this.active = active === "true";
  }

  connectedCallback() {
    this.updateRenderState();
  }

  static get observedAttributes() {
    return ["active"];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (oldValue === newValue) return;
    switch (name) {
      case "active":
        this.active = newValue === "true";
        break;
    }
    this.updateRenderState();
  }

  updateRenderState() {
    if (this.active) {
      this.style.display = "block";
      // The follwing line is import to re-render the content
      // and activate any events that happen when mounting components
      this.innerHTML = this.innerHTML; // eslint-disable-line
    } else {
      this.style.display = "none";
    }
  }
}

class WebLink extends HTMLElement {
  /** @type {string} */
  to;

  constructor() {
    super();
    this.to = this.getAttribute("to");
  }

  connectedCallback() {
    this.tabIndex = 0;
    this.role = "link";
    this.style.cursor = "pointer";
    this.addEventListener("click", (e) => {
      e?.preventDefault?.();
      this.clickNavigate();
    });
  }

  clickNavigate() {
    navigate(this.to);
  }
}

// Definitions and Exports

window.customElements.define("web-router", WebRouter);
window.customElements.define("web-route", WebRoute);
window.customElements.define("web-link", WebLink);

export { navigate, listenNavigation, getParam };
