// This is merely a test component to show that pages are re-mounted as you navigate
class TestRerender extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    console.log("first mounted");
  }
}

window.customElements.define("test-rerender", TestRerender);
