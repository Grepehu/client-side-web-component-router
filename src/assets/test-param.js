// This route has to be based on the final built website
import { getParam } from "/assets/router.js";

// This is merely a test component to show that pages we can get parameters
class TestParam extends HTMLElement {
  /** @type {string} */
  param;

  constructor() {
    super();
    this.param = this.getAttribute("param");
  }

  connectedCallback() {
    this.innerHTML = getParam(this.param);
    this.style.color = "#ff0000";
  }
}

window.customElements.define("test-param", TestParam);
