# Web Router

Web router is a client-side router that allows you to navigate between pages of your application without reloading the page, effectvely turning your website into a **Single Page Application (SPA)**.

The big difference Web Router offers is the fact that it's built purely with native JS Web Components, making it much more stable.

## Instalation

In this repository grab the file `/src/assets/router.js` and copy it to your own project wherever you see fit.

Then add to the end of your HTML body tag script import mapping to this file where you placed it with `type="module"`:

```html
    ...
    <script type="module" src="/path/to/router.js"></script>
  </body>
```

## Basic Usage

Web Router is compose of 3 main components:

- `web-router`: This is the main wrapper of everything, it has to be around everything using the Web Router because it holds all the global data related to the routes and currently active parameters;
- `web-route`: This is where you define one single route, anything inside one of these components is only going to be rendered on the page if the URL matches the pattern set in the attribute `path` for this component. Every change in the URL re-renders the contents of this route to make sure any events tied to the mount components is triggered when the route changes;
- `web-link`: This is the replacemente for the anchor tag, they are links that take you to other pages without reloading the whole website.

A basic example would be:

```html
<nav>
  <web-link to="/">home</web-link>
  <web-link to="/about">about</web-link>
</nav>
<web-router>
  <web-route path="/">
    <h1>Page "/"</h1>
  </web-route>
  <web-route path="/about">
    <h1>Page "/about"</h1>
  </web-route>
</web-router>
```

## Dynamic Routes

There are scenarios where we want to have routes that are dynamic and can be anything inside some specific pattern, for that we also have dynamic routes where you can define dynamic parameters in the `path` for a `web-route` and it's going to match anything in that pattern as well as saving the value it matched with your dynamic pattern:

```html
<web-router>
  <web-route path="/:id">
    <h1>Page with ":id"</h1>
  </web-route>
  <web-route path="/:slug/:id">
    <h1>Page with ":slug" and ":id"</h1>
  </web-route>
</web-router>
```

Now anything with a route on the style `/:id` will match, like for example `/1` and `/23` both match the same `/:id` route and don't worry because `/test/12` would only match `/:slug/:id` and not `:id` because we require the exact number of sections in the URL to consider a match.

With these dynamic routes we may now need to obtain the values matches by these patterns in our pages, for that we have `getParam`. Anywhere in your application you can import this function and get specific values from paramaters that are currently selected:

```js
import { getParam } from "/path/to/router.js";

const slugValue = getParam("slug");
```

And with this you can use the values from the paramaters dynamically inside your application.

## Utility Functions

Much like `getParam`, there are other utility functions you may import from the router to add dynamic behavior to your application based on the router:

- `getParam`: This one as mentioned before is the function that receives a string parameter with same name as the one you named in the `web-route` and returns the current value of it if any, otherwise returns `null`;
- `navigate`: This function receives two parameters, the first one is mandatory which is the URL to where you want to move your URL without reloading the page, this is useful in case you can't simply use `web-link`. The second one is a `state` object where you can pass anything inside of it and this state can be retrieved in the next route using the next function we'll talk about `listenNavigation`;
- `listenNavigation`: At last we have this function that receives a single parameter which is a callback function that is going to be triggered whenever the route changes. The callback function has one paramater which is the `event`, this paramater has all the common event paramater from native JS but they also have an object inside of them called `detail`, this object has two fields, `url` that contains the new URL the route has navigated to and `state` which is whatever was passed down to the `navigate` function when it was used (if nothing was passed to it, then it's an empty object).

## Testing

This repository is also an usage example for this route, you only need to have NodeJS 18+ installed, run `yarn install` in the root folder, then `yarn serve` to run a testing server, where you can check the router being used in real time at `http://localhost:8080`.

Check `/src/index.html` to see where we add the router to be used and also `/src/assets/test-param.js` and `/src/assets/test-rerender.js` to se other web components being affected by the routes changes.

For a more complex example you may also check [Pokemon Vanilla SPA](https://gitlab.com/Grepehu/pokemon-vanilla-spa), a project created using Web Router.